# Aircraft Radio Communications

[Pilot/Controller Virtual Bingo](mfbc.us/m/ff42qq)


## AIM 4-2-1

Accoding to [AIM 4-2-1](https://www.faa.gov/air_traffic/publications/atpubs/aim_html/chap4_section_2.html), "Radio communications are a critical link in the ATC system."

> Student Pilots Radio Identification.
> The FAA desires to help student pilots in acquiring sufficient practical experience in the environment in which they will be required to operate. To receive additional assistance while operating in areas of concentrated air traffic, student pilots need only identify themselves as a student pilot during their initial call to an FAA radio facility.
> EXAMPLE-
> Dayton tower, Fleetwing One Two Three Four, student pilot.
> This special identification will alert FAA ATC personnel and enable them to provide student pilots with such extra assistance and consideration as they may need. It is recommended that student pilots identify themselves as such, on initial contact with each clearance delivery prior to taxiing, ground control, tower, approach and departure control frequency, or FSS contact.

Learn the [phonetic alphabet](https://www.faa.gov/air_traffic/publications/atpubs/aim_html/chap4_section_2.html#TBL_4_2_2).

[Sign up for an O-Ride](http://orides.coloradowingcap.org/).


## Aviation Resources

- [Aeronautical Information Manual](https://www.faa.gov/air_traffic/publications/atpubs/aim_html/index.html)
- [Airplane Flying Handbook, FAA-H-8083-3B](https://www.faa.gov/regulations_policies/handbooks_manuals/aviation/airplane_handbook/media/airplane_flying_handbook.pdf)
- [New Pilot's Guide to ATC Communication - AOPA](https://www.aopa.org/training-and-safety/students/presolo/special/new-pilots-guide-to-atc-communication)
- [LiveATC.net - KAPA](https://www.liveatc.net/search/?icao=APA)
- [LiveATC.net - KDEN](https://www.liveatc.net/search/?icao=DEN)
- [Pilot's Handbook of Aeronautical Knowledge, FAA-H-8083-25B](https://www.faa.gov/regulations_policies/handbooks_manuals/aviation/phak/media/pilot_handbook.pdf)
- [Pilot/Controller Glossary](https://www.faa.gov/air_traffic/publications/media/pcg_basic_w_chg_1_2_7-16-20.pdf)
- [Say It Right: Mastering Radio Communication - AOPA](https://www.aopa.org/lms/courses/say-it-right/#01-effective-radio-communication&01-introduction)
