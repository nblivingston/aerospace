# Rocket Launches

## We are joining CRASH at Ft Lupton this month!

Next launch: __15 May 2021__

Time: __1000__ to __1400__

Uniform: civilian attire

[Weather Forecast](https://www.wunderground.com/hourly/us/co/fort-lupton/date/2021-5-15)

The [Colorado Rocketry Association of Space Hobbyists (C.R.A.S.H.)](http://crashonline.org/) is the Denver area's launch club. They host launches at a new site in Ft Lupton](https://www.google.com/maps/place/40%C2%B005'14.8%22N+104%C2%B044'05.5%22W/@40.085614,-104.7624722,14.22z/data=!4m5!3m4!1s0x0:0x0!8m2!3d40.08743!4d-104.73485). Come check out the new launch site and a variety of different rockets on 15 May. If you have a rocket, bring it and Lt Livingston will provide motors. If you don't have one, come and watch!

## Rocketry Resources
- [Civil Air Patrol Model Rocketry](https://www.capnhq.gov/CAP.AEDownloads.Web/Files/DownloadFile/130)
- [Igniter Installation Instructions](http://crashonline.org/wp-content/uploads/2018/05/igniters.pdf)
- [Make: High-Power Rockets](https://www.makershed.com/products/make-high-power-rockets) by Mike Westerfield. $10.50 from Maker Shed. For information only, not required.
- [Make: Rockets](https://www.makershed.com/products/make-rockets) by Mike Westerfield. $12 from Maker Shed. For information only, not required.
- [NAR Model Rocket Safety Code](https://goo.gl/maps/hLLB7mtuM5KQMEYp7)
