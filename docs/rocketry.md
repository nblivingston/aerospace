# Rocketry

![Rocket Propulsion](images/rocket_propulsion.png) from _Aerospace Dimensions Module 4: Rockets_

[New to Rocketry](https://www.apogeerockets.com/New-to-Model-Rocketry?pg=quickside)

[Phases animation](https://www.apogeerockets.com/Tech/Phases-of-a-Rockets-Flight)

[Parts of a Rocket](https://www.apogeerockets.com/Tech/Parts_of_a_Rocket)

[If Rockets were Transparent](https://www.youtube.com/watch?v=su9EVeHqizY)

[CU Sounding Rocket Laboratory](https://www.colorado.edu/studentgroups/cobra/)
