# Payload-Related Resources

- https://appinventor.mit.edu/

## Bluetooth

- [HC-05 Bluetooth Module](https://create.arduino.cc/projecthub/electropeak/getting-started-with-hc-05-bluetooth-module-arduino-e0ca81)
- [Arduino nan BLE 33 - how to send data](https://forum.arduino.cc/index.php?topic=660532.0)
- [BLE very weak signal](https://forum.arduino.cc/index.php?topic=659562.15)
- https://btprodspecificationrefs.blob.core.windows.net/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf
- https://www.instructables.com/Connect-Arduino-Uno-to-Android-via-Bluetooth/

## LoRa

- https://www.instructables.com/Dragino-LoRa-GPS-Tracker-1/
- [S.O.S Enabled GPS Tracker](https://www.hackster.io/scottpowell69/s-o-s-enabled-gps-tracker-620407#toc-the--home--device-5)
