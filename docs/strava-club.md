# Joining the MHCS Strava Club

The [Mile High Cadet Squadron CO-143](http://www.milehighcadets.co143.org/home) has a club on Strava that cadets may use to record fitness activities.

- These activities may satisfy Civil Air Patrol physical fitness requirements.
- The Strava website, app, and club are free to use.
- Participation in the MHCS Strava club is optional.

Follow the instructions below to register an account and join the MHCS club. Please pay close attention to the steps to maximize cadet protection and privacy.


## Register on Strava

You will need an account on Strava to join the club. Register an account by visiting [the Strava website](https://www.strava.com/) and choosing a sign up option.

The squadron recommends using your Civil Air Patrol email to register. If you already have a profile on Strava, you do not need to register again. Also, if you cannot easily access your CAP account, another personal account is fine. Do not register using Facebook as we want to protect your privacy as much as practical.

When registering:

- __Cadets must not include their full name in their profile.__ Your profile name should be your first name or nickname followed by your last initial, for example "Nick L". If you already have a profile on Strava, please modify it to display only your first name and last initial. Senior members may choose to display their full name.
- __Cadets must not include a photo of themselves in their profile.__ You may use a profile photo, but it must not be of a person. Senior members may include a profile photo of themselves.
- In the profile settings, select `Privacy Controls` on the left and click the hard-to-see down arrow and choose __Followers__ or __Only You__. We may need to adjust this setting if it prevents your activities from appearing to club members.
- Create a _Privacy Zone_ for your address. This will prevent your home from being identified in your workout maps on the site. You will need to enter your home address on the site, then choose a radius around it. Our current guidance is to select 1/4 mile, although we may update this in the future.
- __Uncheck__ the box that says "Include your activities in Metro and Heatmap".


## Join the Club

You must request access to join the MHCS club. To request access:

- If you are on a computer, visit [the MHCS club link](https://www.strava.com/clubs/mhcs) and click _Request to join this club_ under "Club Leaderboard."
- If you are using a phone, open the Strava app, tap the "Explore" icon (it looks like a compass), then search with the magnifying glass icon for "MHCS". Tap on the MHCS club and then tap "Request to Join".

For cadet protection, __cadets and senior members should not follow each other outside of the club__. There are exceptions to this rule. If it would violate the principles of cadet protection for a particular cadet to ride alone in a car with a particular senior member, neither should follow the other on Strava.

To let us know you've requested access, or for technical assistance with using the site, the app, or pairing with fitness devices, please post a comment in the squadron's Microsoft Teams chat channel.
