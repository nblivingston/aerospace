# Crew Dragon Astronaut Test Flight, 30 May 2020

> "For the first time in nine years, NASA astronauts were launched from American soil on an American rocket, and for the first time in history, those astronauts flew on a commercially built _and_ operated spacecraft."[1]

<img src="https://mk0spaceflightnoa02a.kinstacdn.com/wp-content/uploads/2020/06/NASA-DM2-7060.jpeg" width="600" align="center"/>

_Falcon 9 and Crew Dragon blast off from pad 39A_[2]

Video: [Inside KSC! for June 5, 2020](https://youtu.be/xhrJzj0Y7C8)

NASA provides information about [Commercial Crew Program history](https://www.nasa.gov/specials/ccp-press-kit/main.html) leading up to Demo-2 launch.


## Launch

> "The SpaceX Crew Dragon spacecraft carrying NASA astronauts Robert Behnken and Douglas Hurley lifted off at 3:22 p.m. EDT on May 30, 2020, from Launch Complex 39A at NASA Kennedy Space Center in Florida. The spacecraft was launched atop a reusable SpaceX Falcon 9 rocket... Behnken and Hurley named their spacecraft Endeavour as a tribute to the first space shuttle that both astronauts had flown aboard. Endeavour also flew the penultimate mission of the Space Shuttle Program, launching in May 2011 from the same pad."[1]

Video: [SpaceX Falcon 9 with Crew Dragon Lifts Off](https://youtu.be/xhrJzj0Y7C8) - 1 minute, 35 seconds


## Docking with the International Space Station

![Crew Dragon Prior to Docking](images/iss063e021578_lrg.jpg) _Crew Dragon just prior to docking with the ISS_[5]

Video: [Crew Dragon Separates from SpaceX Falcon 9 Rocket](https://youtu.be/I7P2VJj3UqM) - 35 seconds

See "[SPACE STATION](https://www.spacex.com/human-spaceflight/iss/index.html)" on the SpaceX website for an interactive infographic about a typical approach to the space station.

Also check out the SpaceX [ISS Docking Sim](https://iss-sim.spacex.com/).


## The Falcon 9 Rocket

> "Falcon 9 is a partially reusable two-stage-to-orbit medium-lift launch vehicle designed and manufactured by SpaceX in the United States. It is powered by Merlin engines, also developed by SpaceX, burning cryogenic liquid oxygen and rocket-grade kerosene (RP-1) as propellants. Its name is derived from the fictional Star Wars spacecraft, the _Millennium Falcon_, and the nine Merlin engines of the rocket's first stage."[3]

SpaceX provides [more information about the Falcon 9](https://www.spacex.com/vehicles/falcon-9/), including a flight video, a bit about the Merlin engines, and a [user's guide](https://www.spacex.com/media/falcon_users_guide_042020.pdf).


## The Dragon Vehicle

> "The Dragon spacecraft is capable of carrying up to 7 passengers to and from Earth orbit and beyond. The pressurized section of the capsule is designed to carry both people and environmentally sensitive cargo. Toward the base of the capsule and contained within the nosecone are the Draco thrusters, which allow for orbital maneuvering."[4]

The SpaceX [page for the Crew Dragon](https://www.spacex.com/vehicles/dragon/index.html) also has a [video of the Crew Dragon Interior](https://youtu.be/78ATfCaBn6E).

Wikipedia has [more information about the Dragon vehicle](https://en.wikipedia.org/wiki/Dragon_2).


## The Astronauts
For brief biographies of the astronauts, see the [NASA Demo-2](https://www.nasa.gov/specials/dm2/) site.


# The Commercial Crew Program
NASA provides [an overview of the Commercial Crew Program](https://www.nasa.gov/content/commercial-crew-program-the-essentials).


## References
- [1](https://earthobservatory.nasa.gov/images/146794/astronauts-launch-from-american-soil), _Astronauts Launch from American Soil_, earthobservatory.nasa.gov, 30 May 2020
- [2](https://spaceflightnow.com/2020/05/31/photos-falcon-9-and-crew-dragon-blast-off-from-pad-39a/) - _Photos: Falcon 9 and Crew Dragon blast off from pad 39A_, spaceflightnow.com
- [3](https://en.wikipedia.org/wiki/Falcon_9) - _Falcon 9_ on Wikipedia
- [4](https://www.spacex.com/vehicles/dragon/index.html) - spacex.com, _Dragon: Sending Humans and Cargo into Space_
- [5](https://www.nasa.gov/image-feature/crew-dragon-approaches-the-international-space-station/), www.nasa.gov, 3 June 2020


## Additional Links
- [Launch Video](https://youtu.be/xY96v0OIcK4) - Full launch coverage. Note that the booster landed at 4:32:30
- [NASA Commercial Crew Program](https://www.nasa.gov/exploration/commercial/crew/index.html
- [NASA Demo-2](https://www.nasa.gov/specials/dm2/)
- [SpaceX Demo-2](https://www.flickr.com/photos/nasahqphoto/albums/72157714384169073) - NASA flickr album
- [SpaceX's historic Demo-2 Crew Dragon astronaut test flight: Full coverage](https://www.space.com/spacex-crew-dragon-demo-2-test-flight-explained.html), space.com, 31 May 2020
- [SpaceX’s reusable Falcon booster returns to port after crew launch](https://spaceflightnow.com/2020/06/02/photos-spacexs-reusable-falcon-booster-returns-to-port-after-crew-launch/)
