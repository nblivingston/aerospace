To complete the technician rating in the AESpecialty Track, the member must:
- Explain the appointment process for the specialty track.
- Describe the knowledge, performance and service requirements of the AE Specialty Track.
- Review AE programs and products, including AE STEM Kits, provided by National Headquarters with the trainer/mentor.
- Define   the   Aerospace   Education   Excellence   (AEX)   hands-on   award program and how to implement it in squadrons and schools.
- Outline the selection and recruitment procedures of Aerospace Education Members (AEMs), describe the Aerospace Connections in Education (ACE) program  in  schools  and  explain  the  Teacher  Orientation  Program  (TOP) Flights for teachers.
- Explain the difference between earned and nominated awards.
- Demonstrate knowledge about formulating budgets, obtaining grants, and funding of AE activities.
- Describe  the  preparation  of  an  online  AE  Plan  of  Action  (AE  POA)  for  a squadron.
- Describe the  preparation  of  an  online  annual AE Activity  Report  for  a squadron.
- Describe the subordinate unit inspection (SUI) process for a squadron.
- Successfully complete the online technician level test in eServices.

Compile an AE Notebook:
1. Appointment of AEOs
CAPR 35-1: Assignment  Actions. Duty  positions  are  normally  assigned  by  using  the  on-line  duty  assignment  application available in eServices.  Only the basic duty positions outlined in CAPR 20-1, Organization of Civil  Air  Patrol,  are  reflected  in  the  on-line  application. Duty  assignments  may  be  initiated  by  the  personnel officer or any member designated by the unit commander and provided permission by the Web Security Administrator.  The individual’s record will be automatically updated to reflect the new position.  An  automatic  e-mail  will  be  forwarded  to  both  the  individual  assuming  the  new  duty  position  and  the  individual leaving the  position  reminding  them  of  the  responsibility  to  properly  account  for  the  records,  property and assets of this position.  A copy of the online duty assignment screen may be printed to place in the member’s personnel file.a.Commanders  not  wishing  to  use  the  on-line  application  submit  a  CAPF  2a,  Request  for  and  Approval of Personnel Actions,  Section II, Duty Assignment Change.  The personnel officer initiates the personnel action which is approved by the unit commander and forwards the form to NHQ/DP.  Both the individual assuming the new duty position and the individual leaving the position must sign the CAPF 2a signifying that the records, property and assets for this position are property accounted for.  A copy will be filed in the individual member's personnel file and a copy retained by the personnel officer.  b.As  an  option  to  the  preparation  of  separate  CAPFs  2a  for  each  duty  position  assigned,  duty  assignment changes may be reflected in published personnel authorizations in accordance with CAPR 10-3, Administrative Authorizations.   A copy of the personnel authorization will be filed in each individual’s personnel  file. Commanders  choosing  to  use  this  option  must  ensure  that  the  individuals  concerned  are  aware of their responsibility to properly account for the records, property and assets of the position.  c.Commanders who have implemented additional duty positions or titles not reflected in the on-line application must prepare CAPFs 2a or published personnel authorizations to assign members to these positions; however, this information is not forwarded to National Headquarters and the assignments will not  appear  in  the  on-line  record. Assignments  to  committees,  boards,  etc.,  or  special  one-time  duty  assignments  do  not  require  a  CAPF  2a,  but  will  be  reflected  in  published  personnel  authorizations.   A copy of the CAPF 2a or personnel authorization will be filed in each individual’s personnel file

** organiztion of CAP is actually CAPR 30-1

The applicable regulations regarding appointments and duty assignments are CAPR50-1 (AE regulation); CAPR 20-1(I);CAPR 35-1, paragraph1-3, a, b, c (speaks to appointment process). Other regulations may prove to be helpful, but these are the most likely references that apply to the AEO appointment process. The AE Specialty Track is described in CAPP50-2. One of the first things you will need to do is assure that you are officially appointed in writing to the position. This step can be overlooked, or not properly documented, and should not be, because your time-in-service (six months-Technician and Senior, or one year -Master) begins on the date of appointment.  It is always smart for a CAP member to follow their individual records and assignments to make sure everything is correct and up to date. Check member records in eServices. Though much improved and more reliable, the records system is still occasionally flawed.

There are three primary ways to accomplish the formal assignment in writing. Commanders not wishing to use the on line application (or not having electronic resources available) should submit a CAPF 2a regarding personnel actions to National HQ. (This can be done by fax, e-mail or, if necessary, by mail.) The CAPF2a is a multi-use form and also allows documentation of transfer of pertinent records, assets etc. (notebooks; stem kits) at the time of the appointment. A copy of the completed CAPF2a should be placed in the member’s personnel file.Many Commanders prefer a personnel authorization (PA) as a means of simultaneously appointing all the duty assignments in a squadron, group or wing. A PA is essentially a letter listing all the available positions and the names of the individuals filling each slot. A copy of this is placed in each member’s file and one is retained by the personnel officer.  A copy is also often posted on a bulletin board at the unit’s meeting space. With the PA appointment method, the appointee should make sure the assignment is entered into the official files maintained at National HQ. The unit personnel officer should ensure that each track assignment is forwarded to National eServices to be included in the member’s personnel records.The third method is for the Commander and personnel officer to enter the individual’s AE specialty track assignment directly into e-services. The advantage with this is that each member is assured that their assignment is recorded at National HQ. The disadvantage is that there is often no visible record of the assignment at the unit meeting space, though such a list could be created if necessary

2. AEO Specialty Track

CAPP 50-1 p. 15: In summary, what is required for the AE Specialty Track technician rating?
 Gain AE knowledge from all references and pass the online AE specialty track technician
knowledge quiz.
 Develop and use an AE Notebook.
 Serve 6 months in an AEO position.
Within the AE specialty track, what is required to achieve the senior and master ratings? For the AE
senior rating, the following is required:
 Gain more knowledge and pass the online AE senior rating specialty track knowledge quiz.
 Perform the required AE senior tasks.
 Serve in an AEO position for 6 months.
For the master rating, it is more of the same:
 Gain more knowledge and pass the online AE master rating specialty track knowledge quiz.
 Perform the required AE master tasks.
 Serve in an AEO position for 12 months.

CAPP 50-1 p. 16: Squadron/Group AEO:
- Responsible for all AE programs in the unit (cadet and senior if applicable)
- Communicates and coordinates with the commander to develop unit AE goals and objectives and
annual budget
- Reports periodically to unit commander and group AEO, or wing DAE on AE program
- Selects, trains and supervises AE assistants to help perform AE responsibilities
- Working toward the next higher rating level in the AE Specialty Track
- Initiates and supports external AE programs in the community
- Supports group/wing/region/national internal and external AE activities
- Encourages unit and school participation in the Model Rocketry program, the AE Excellence (AEX)
program, and the AE STEM Kit program
- Encourages and helps cadets to complete AE requirements
- Promotes the Teacher Orientation Program (TOP) Flights; working with DAE as Point of Contact
(PoC)
- Has completed or working on completion of the Yeager test
- Administers and evaluates the Yeager test for senior members. (Test Control Officer can also
administer the test.)
- Applies for AFA/CAP unit AE grants
- Develops the annual squadron AE Plan of Action
- Completes the AE Activity Report
- Files reports as required by regulations
- Maintains an AE Notebook; paper or digital

3. AE/STEM Programs and Products

CAPP 50-1 p. 19: CAP AE has over 40 curriculum products in our inventory to share with our members. We have
products for all ages, pre-K through 12 and beyond, and in a wide variety of aerospace and STEM areas.
When talking to schools, principals or teachers, AEOs should tell educators that our products meet
national standards and easily can be integrated into their content skills and classroom instruction. Most
of our products contain hands-on activities, which highlight the importance of learning by doing. We all
learn more by seeing, hearing about and then doing an activity. Involving students in hands-on activities
reinforces the learning and enhances the experience for the cadet or student.

CAP AE has more than 500 hands-on aerospace and STEM educational activities from which to
choose, which provides for relevance and real-life applicability of the activities/lessons. And, of course, there are many other wonderful activities available through other organizations which we try to
promote on our AE Resources webpage. Feel free to use whatever works best for you and your cadets to
further promote AE and STEM, but you should be familiar with all the CAP products that are available to
you.

CAPP 50-1 p. 20: Programs include:
- Aerospace Connections in Education (ACE): for CAP educator members in grades K-6
- Aerospace Education Excellence Award Program (AEX): hands-on aerospace activity participation and awards program for
cadets, senior members, and educator members.
- Air Force Association (AFA) STEM Programs for CAP Units: CyberPatriot and
StellarXplorer.

CyberPatriot is the National Youth Cyber Education Program. At the center of CyberPatriot is the
National Youth Cyber Defense Competition. The competition puts teams of high school and middle
school students in the position of newly hired IT professionals tasked with managing the network of a
small company. In the rounds of competition, teams are given a set of virtual images that represent
operating systems and are tasked with finding cybersecurity vulnerabilities within the images and
hardening the system while maintaining critical services in a six-hour period. Teams compete for the top
placement within their state and region, and the top teams in the nation earn all-expenses-paid trips to
Baltimore, Md., for the National Finals Competition where they can earn national recognition and
scholarship money.

StellarXplorers inspires and attracts teams of two-six high school students to pursue studies and
careers in STEM through a challenging, space system design competition involving all aspects of system
development and operation with a spacecraft/payload focus. The teams are each lead by a Team
Director, with optional Team Mentors.
STLX uses Systems Tool Kit (STK) by Analytical Graphics to conduct this national competition. This
should be especially interesting to CAP cadets due to the fact that CAP has integrated STK in its AE
programs for many years. CAP’s AE page for STK activities is found at
https://www.gocivilairpatrol.com/programs/aerospace-education/programs/systems-tool-kit/.

- Model Rocketry Program and Advanced Rocketry: The program consists of three progressively challenging phases. Each phase has a written section, short quiz, and a hands-on section. CAP also participates in the Team America Rocketry Challenge (TARC), in which teams compete to see who can launch rockets to the highest altitude.

- AE STEM Kit Program: generates enthusiasm among cadets and K-12 youth for STEM-related
subjects and careers through a program supplying STEM resources for hands-on, inquiry-based
learning.

- Systems Tool Kit (STK): Software that supports satellite systems from mission planning through operations. Daily licenses are available for AEOs.

- Teacher Orientation Program (TOP) Flights: can provide a flight for any CAP educator member.

4. AEX
CAPR 50-1 8.1 Aerospace Education Excellence (AEX) Program. The AEX program is hands-on, inquiry-based learning tool that meets national academic standards and provides aerospace, aviation, space, and STEM- related activities to our members. It is both a participation and award program. Members may use the AEX books and may also use any of the other AE/STEM products to conduct AE activities to satisfy the requirements of the AEX award.

CAPP 50-1 p. 27: In order to be eligible for the award portion of the program, unit participants must complete six
activities and a two-hour AE field experience within the fiscal year. Members must apply for AEX before they can submit an AEX Award Report.

5. AEMs, ACE and Top Flight
CAPR 50-1 6.1: Aerospace Education Member (AEM). AEM is a special membership category open to educators or any reputable individual or organization that has a desire to promote aerospace education. AEMs do not attend CAP meetings on a routine basis or wear the CAP uniform. However, AEMs have access to CAP AE products, help develop and implement AE materials, and promote aerospace education and STEM to their students, peers and the general public.

CAPR 50-1 8.4. Aerospace Connections in Education (ACE). ACE is CAP’s K-6 AE program that provides teachers with grade-specific lesson plans on character development, physical fitness and academics, all revolving around an aerospace theme. AEOs are encouraged to connect their squadrons with ACE teachers to assist in the implementation of the program, and to introduce the cadet program to fifth and sixth graders. To learn more about the program and how to involve teachers, refer to CAPP 50-1, chapter 5.

CAPR 50-1 8.5. Teacher Orientation Program (TOP) Flight. The TOP Flight program provides an opportunity for teacher members, both AEMs and senior members who are teachers, to experience the thrill of flying with an orientation flight in a CAP aircraft. AEMs who contact CAP/AE will be put in contact with the appropriate wing DAE, who will either coordinate the flight with the AEM or find someone in the local area to coordinate the flight details. The teachers, CAP/AE, a wing DAE, the pilot or pilots, the Wing Director of Operations, and the Wing Commander must all be involved to ensure a successful TOP Flight. An overview of the program and the necessary procedures can be found in the TOP Flight Handbook located on the AE website.

6. AE Awards

CAPR 50-1 9. AE awards can be divided into two general categories – earned and nominated. Earned awards result from a member completing activities, performing duties, or taking tests. Nominated awards result from a nomination package submitted on behalf of any individual. Refer to CAPP 50-1, chapter 6. CAPF 120, Recommendation for Decoration, will be used only as the cover sheet for each AE award nomination, filling in only the personal data or unit data sections. Use appropriate AE forms, CAPF 50-1 through CAPF 50-4, to complete all AE award nomination packets. Any CAP member may submit an AE award nomination.

CAPR 50-1 9.1.1. AEX Award Program. Each participant who successfully completes six AE/STEM related hands-on activities, plus completes an additional two-hour activity, has met the requirements of this program and will receive a completion certificate. The activities can be CAP activities, or they can come from other sources if they are AE/STEM related activities. The additional two hours can be anything related to AE/STEM. For instance, it can be an activity, a combination of activities, or a field trip.

CAPR 50-1 9.1.4. Squadron AE Achievement Award. The Squadron AE Achievement Award identifies squadrons that successfully perform at least eight of the eighteen criteria, which encompass both internal and external programs in AE.

Four of the tasks are required. The internal tasks are AEX, Model Rocketry, STEM Kits, Yeager Award (75% of SMs), an AE Award nomination, AEO school, Wing Conference seminar/presentation. The external tasks are AEM recruitment, AEM activities, AEM STEM Kits, ACE assistance, Youth/Civic Group AE Activity/Presentation, TOP Flight, School Visit. Four of the additional tasks are required, with at least one internal and one external.

7. Budget, Grants and Funding

CAPP 50-1 p. 49: The fiscal guidance
regulations are currently numbered CAPR 173 - 1 (Finance) and CAPR 173 - 4 (Fundraising and
Donations).

- Fundraising: the Wing Commander must be involved
and aware of your fundraising project. He/she will have to give permission in writing for you to proceed.
There are many things that are allowed but none can be conducted without this written permission. The
letter may be communicated electronically or by mail.

There will be no Air Force involvement in CAP
fundraising. CAP and Air Force are to be kept as separate entities. No wearing of the Air Force style CAP
uniform is allowed except by cadets, under certain circumstances. Any promotional matter should
clearly identify CAP as a distinct organization from the Air Force.

Do not forget to include the Wing Legal Officer. This is particularly important as the date for your
event comes closer to “it’s a go.” The legal officer can double check your plans and assure that no small
oversight will cause a problem.

- Donations: donations to CAP are considered to be charitable contributions. Donations can cross a wide range of items and activities. Items such as computers, motor vehicles,
mobile home trailers (with the intent that it be used as meeting space for a CAP unit, for example),
boats or aircraft require a donation receipt (CAPF 164), but may also require additional IRS documents.
Check with the finance officer or Wing legal officer. There are rules regarding the disposal of such
donations. Check CAPR 173-4. Donations of property (land) also have special applicable rules and are
regulated as to their possible disposal. (See above) Donations of money and bequests of funds are
treated as above and require a donation receipt.
In that they are often unplanned and unscheduled, donations are still a welcome contribution, but
require a set of procedures that need to be followed. Any donation valued at $250 or more requires a
donation receipt. Under certain circumstances, that valuation may drop to $75. CAPR 173-4 Attachment
1, Donation Receipt, Form 164.
CAP units may accept donations through a Combined Federal Campaign (Federally based) or United
Way Campaign, usually community based. The need here is to make the group aware of CAP in general
or particularly a local unit and that they are tax-exempt. This has proven profitable for some units. So,
unit representatives should plan on making presentations to these groups as their fall season develops.

- Grants: The rules for obtaining grants are relatively simple. Be very specific in your request. Keep it as narrow
as possible so the proper focus remains in place. Follow all the grant guidelines to the letter. Be persistent but polite. Answer any follow-up questions promptly. At the conclusion, even if the grantor does not require a summary
document, write a thank you letter. A community relations person, CAP public affairs officer, etc.,
should be included to coordinate grant applications. It is recommended that anyone involved in unit funding refer to CAPR 173-4 Section C. This portion
of the regulation very specifically lists what is not permitted for any funding effort. It also lists
specifically allowed fund raising activities. The Air Force Association provides funding to CAP to provide quarterly $250 grants to CAP units and CAP educator members to promote aerospace and STEM education.

8. AE POA

CAPR 50-1 10. Each wing DAE will develop an AE Plan of Action (AE POA) in eServices each year. This plan represents the AE programs and activities the wing plans to conduct in the upcoming year. The plan will consist of four parts: 1) Focus the AEOs on accomplishing the squadron AE Achievement Award; 2) Remind the squadron AEOs to complete the AE Annual Activity Report at the end of each fiscal year; 3) Indicate on the AE POA in eServices, in the areas provided, the activities the wing plans to complete; and 4) Complete the Above and Beyond section of the plan for additional areas, events, or activities the wing conducted that are not covered in part three. The Wing Commander will approve the AE POA in eServices. Regions will review the wing AE POA. Refer to Attachment 2 of this regulation for submission deadlines and for additional information refer to CAPP 50-1.

9. AE Activity Report

CAPR 50-1 11. Wing DAEs are responsible for monitoring and tracking the completion of squadron AE Annual Activity Report in eServices. The report records all the unit’s accomplishments for the past fiscal year. The wing DAE will consolidate the inputs to complete the wing AE Annual Activity Report. The Commander will approve the report. Regions will then review the wing submissions. Refer to Attachment 2 for submission deadlines. For more information, refer to CAPP 50-1 of this regulation.

10. Inspection Preparation

CAPP 50-1 p. 66: The CAP Regulation governing the inspection process is explained in great detail in CAPR 20-3. Also see the Inspection Knowledge Base (IKB) in the Inspector General module.

The SUI is a review of a unit’s program management, mission accomplishment and regulatory
compliance. The AE program has been greatly simplified with the availability of electronic filing of the
Plan of Action and Activity Report. SUIs are expected to occur every 24 months. Between SUIs, a unit
conducts a self-inspection. The questions remain the same as for an SUI. If a unit exceeds 27 months
between SUI visits, the Wing Commander must suspend the unit’s activities until the SUI can be
completed. For the exact schedule of a unit SUI, there is an inspection program plan of action for all the
subordinate units each year developed by the IG with the concurrence of the Wing Commander.

CAPR 50-1 12. CAP wings and squadrons will be inspected to ensure the AE mission is being conducted
in compliance with this regulation.

CAPR 50-1 12.2. Subordinate Unit Inspections – SUI. Squadrons and groups will be inspected regularly, applying the questions and procedures regarding the SUI found in Attachment 1 of this regulation.

CAPR 50-1 Attachment 1 - compliance elements:
- Did the AEO and the commander discuss future AE activities for the upcoming year? AEO will provide notes or audio/video record of discussion between AEO and CC about future AE activities.
- Does the unit have an Internal Aerospace Education program? Unit will submit examples of AE events (presentations, workshops, other activities, etc.) agendas which will include date and topic presented, activity done, etc.
- Does the unit have an external Aerospace Education program? Unit will submit examples of cooperative events with either schools or community organizations, such as civic or youth groups, which resulted in the promotion of Aerospace Education.
NOTE: If unable to provide documentation then provide documentation of attempts to establish cooperative events to promote Aerospace Education.
- Did the unit complete an AE Annual Activity Report IAW CAP regulations? Unit will submit a copy of the AE Annual Activity Report.
- Are AEOs assigned to the duty position enrolled in the AE specialty track, unless they have already achieved the AE master rating? Unit will provide a list of AEOs enrolled in the AE specialty track from Member Reports.
