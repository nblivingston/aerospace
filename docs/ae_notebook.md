# Aerospace Education Notebook
This digital notebook addresses the Performance Requirement to "Compile an AE Notebook, in either a paper notebook or digital format, based on the below listed topics", as described in [CAPP 50-2 Aerospace Education Specialty Track Guide](https://www.gocivilairpatrol.com/media/cms/P0502_Aerospace_Ed_Specialty_Track__D2FE570D84B79.pdf).

Much of the information in this notebook is derived from [CAPP 50-1 Aerospace Education Officer Handbook](https://www.gocivilairpatrol.com/media/cms/P0501_Aerospace_Education_Officer_H_15FABFAE3F4CC.pdf).

## 1. Appointment of AEOs
[CAPR 35-1 Assignment and Duty Status](https://www.gocivilairpatrol.com/media/cms/R035_001_88C55F858019C.pdf) states, "Duty positions are normally assigned by using the on-line duty assignment application available in eServices... Duty assignments may be initiated by the personnel officer or any member designated by the unit commander and provided permission by the Web Security Administrator." Alternatively, the CAPF 2a or a personnel authorization may be used.

## 2. AEO Specialty Track
CAPP 50-2 describes the expectations for the technician, senior, and master levels. When the commander is satisfied that the AEO can perform to the level required for the rating and has met the service requirements, the commander or the PDO records the award of the rating through the eServices Specialty Track Module as well as in the member’s local personnel record.

The Aerospace Education Officer (AEO) at the __technician__ level is expected to:

- Possess a basic knowledge of the Aerospace Education Programs.
- Know the duties and tasks associated with CAP’s squadron and group AEO and assistant AEO positions.
- Implement aerospace education programs for cadets and senior members at the squadron level and promote AE in local schools.
- Develop into a highly motivated and trained AEO who will conduct and promote effective internal and external aerospace education programs.

The Aerospace Education Officer (AEO) at the __senior__ level is expected to:

- Comprehend the duties and tasks associated with CAP’s squadron, group, wing and region aerospace education positions.
- Implement the elements of the various aerospace education programs.
- Implement AE activities within the unit.
- Promote AE programs for the local community with organizations such as civic organizations and schools.
- Perform successfully in a leadership role within AE.
- Perform other AEOfficer related duties as required.

The Aerospace Education Officer (AEO) at the __master__ level is expected to:

- Demonstrate comprehension of the duties and responsibilities associated with CAP’s squadron, group, wing and region AE positions.
- Demonstrate comprehension of how to manage unit CAP AE programs.
- Successfully implement AE activities and duties as it pertains to their unit.
- Successfully perform in an AE leadership role.
- Provide aerospace education policy and activity recommendations to their commander.
- Develop programs that enhance CAP aerospace education in the community.

## 3. AE/STEM Programs and Products
According to CAPP 50-1, CAP offers over 40 Aerospace Education curriculum products and more than 500 hands-on aerospace and STEM educational activities. These include "products for all ages, pre-K through 12 and beyond, and in a wide variety of aerospace and STEM areas. When talking to schools, principals or teachers, AEOs should tell educators that our products meet
national standards and easily can be integrated into their content skills and classroom instruction. Most of our products contain hands-on activities, which highlight the importance of learning by doing."

The programs described in CAPP 50-1 include:
- Aerospace Connections in Education (ACE): for CAP educator members in grades K-6
- Aerospace Education Excellence Award Program (AEX): hands-on aerospace activity participation and awards program for
cadets, senior members, and educator members.
- Air Force Association (AFA) STEM Programs for CAP Units: CyberPatriot and
StellarXplorer.

CyberPatriot is the National Youth Cyber Education Program. At the center of CyberPatriot is the National Youth Cyber Defense Competition. The competition puts teams of high school and middle school students in the position of newly hired IT professionals tasked with managing the network of a small company. In the rounds of competition, teams are given a set of virtual images that represent
operating systems and are tasked with finding cybersecurity vulnerabilities within the images and hardening the system while maintaining critical services in a six-hour period. Teams compete for the top placement within their state and region, and the top teams in the nation earn all-expenses-paid trips to
Baltimore, Md., for the National Finals Competition where they can earn national recognition and scholarship money.

StellarXplorers inspires and attracts teams of two-six high school students to pursue studies and careers in STEM through a challenging, space system design competition involving all aspects of system development and operation with a spacecraft/payload focus. The teams are each lead by a Team Director, with optional Team Mentors. STLX uses Systems Tool Kit (STK) by Analytical Graphics to conduct this national competition. This should be especially interesting to CAP cadets due to the fact that CAP has integrated STK in its AE programs for many years. CAP’s AE page for STK activities is found at https://www.gocivilairpatrol.com/programs/aerospace-education/programs/systems-tool-kit/.

- Model Rocketry Program and Advanced Rocketry: The program consists of three progressively challenging phases. Each phase has a written section, short quiz, and a hands-on section. CAP also participates in the Team America Rocketry Challenge (TARC), in which teams compete to see who can launch rockets to the highest altitude.

- AE STEM Kit Program: generates enthusiasm among cadets and K-12 youth for STEM-related subjects and careers through a program supplying STEM resources for hands-on, inquiry-based learning.

- Systems Tool Kit (STK): Software that supports satellite systems from mission planning through operations. Daily licenses are available for AEOs.

- Teacher Orientation Program (TOP) Flights: can provide a flight for any CAP educator member.

## 4. AEX
CAPR 50-1 8.1 describes the Aerospace Education Excellence (AEX) Program: "The AEX program is hands-on, inquiry-based learning tool that meets national academic standards and provides aerospace, aviation, space, and STEM-related activities to our members. It is both a participation and award program. Members may use the AEX books and may also use any of the other AE/STEM products to conduct AE activities to satisfy the requirements of the AEX award."

CAPP 50-1 p. 27 notes, "In order to be eligible for the award portion of the program, unit participants must complete six activities and a two-hour AE field experience within the fiscal year. Members must apply for AEX before they can submit an AEX Award Report."

## 5. AEMs, ACE and Top Flight
CAPR 50-1 6.1: Aerospace Education Member (AEM). AEM is a special membership category open to educators or any reputable individual or organization that has a desire to promote aerospace education. AEMs do not attend CAP meetings on a routine basis or wear the CAP uniform. However, AEMs have access to CAP AE products, help develop and implement AE materials, and promote aerospace education and STEM to their students, peers and the general public.

CAPR 50-1 8.4. Aerospace Connections in Education (ACE). ACE is CAP’s K-6 AE program that provides teachers with grade-specific lesson plans on character development, physical fitness and academics, all revolving around an aerospace theme. AEOs are encouraged to connect their squadrons with ACE teachers to assist in the implementation of the program, and to introduce the cadet program to fifth and sixth graders.

CAPR 50-1 8.5. Teacher Orientation Program (TOP) Flight. The TOP Flight program provides an opportunity for teacher members, both AEMs and senior members who are teachers, to experience the thrill of flying with an orientation flight in a CAP aircraft. AEMs who contact CAP/AE will be put in contact with the appropriate wing DAE, who will either coordinate the flight with the AEM or find someone in the local area to coordinate the flight details. The teachers, CAP/AE, a wing DAE, the pilot or pilots, the Wing Director of Operations, and the Wing Commander must all be involved to ensure a successful TOP Flight. An overview of the program and the necessary procedures can be found in the TOP Flight Handbook located on the AE website.

## 6. AE Awards
CAPR 50-1 9. AE awards can be divided into two general categories – earned and nominated. Earned awards result from a member completing activities, performing duties, or taking tests. Nominated awards result from a nomination package submitted on behalf of any individual. Refer to CAPP 50-1, chapter 6. CAPF 120, Recommendation for Decoration, will be used only as the cover sheet for each AE award nomination, filling in only the personal data or unit data sections. Use appropriate AE forms, CAPF 50-1 through CAPF 50-4, to complete all AE award nomination packets. Any CAP member may submit an AE award nomination.

### AEX Award Program
CAPR 50-1 9.1.1. Each participant who successfully completes six AE/STEM related hands-on activities, plus completes an additional two-hour activity, has met the requirements of this program and will receive a completion certificate. The activities can be CAP activities, or they can come from other sources if they are AE/STEM related activities. The additional two hours can be anything related to AE/STEM. For instance, it can be an activity, a combination of activities, or a field trip.

### Squadron AE Achievement Award
CAPR 50-1 9.1.4. The Squadron AE Achievement Award identifies squadrons that successfully perform at least eight of the eighteen criteria, which encompass both internal and external programs in AE.

For the squadron award, four of the tasks are required. The internal tasks are AEX, Model Rocketry, STEM Kits, Yeager Award (75% of SMs), an AE Award nomination, AEO school, Wing Conference seminar/presentation. The external tasks are AEM recruitment, AEM activities, AEM STEM Kits, ACE assistance, Youth/Civic Group AE Activity/Presentation, TOP Flight, School Visit. Of the four tasks, at least one must be internal and one external.

## 7. Budget, Grants and Funding
CAPP 50-1 p. 49: The fiscal guidance regulations are currently numbered CAPR 173 - 1 (Finance) and CAPR 173 - 4 (Fundraising and Donations).

### Fundraising
The Wing Commander must be involved and aware of your fundraising project. He/she will have to give permission in writing for you to proceed. There are many things that are allowed but none can be conducted without this written permission. The letter may be communicated electronically or by mail.

There will be no Air Force involvement in CAP fundraising. CAP and Air Force are to be kept as separate entities. No wearing of the Air Force style CAP uniform is allowed except by cadets, under certain circumstances. Any promotional matter should clearly identify CAP as a distinct organization from the Air Force.

Do not forget to include the Wing Legal Officer. This is particularly important as the date for your event comes closer to “it’s a go.” The legal officer can double check your plans and assure that no small oversight will cause a problem.

### Donations
Donations to CAP are considered to be charitable contributions. Donations can cross a wide range of items and activities. Items such as computers, motor vehicles, mobile home trailers (with the intent that it be used as meeting space for a CAP unit, for example), boats or aircraft require a donation receipt (CAPF 164), but may also require additional IRS documents. Check with the finance officer or Wing legal officer. There are rules regarding the disposal of such donations. Check CAPR 173-4. Donations of property (land) also have special applicable rules and are regulated as to their possible disposal. Donations of money and bequests of funds are treated as above and require a donation receipt. In that they are often unplanned and unscheduled, donations are still a welcome contribution, but require a set of procedures that need to be followed. Any donation valued at $250 or more requires a donation receipt. Under certain circumstances, that valuation may drop to $75.

CAP units may accept donations through a Combined Federal Campaign (Federally based) or United Way Campaign, usually community based. The need here is to make the group aware of CAP in general or particularly a local unit and that they are tax-exempt. This has proven profitable for some units. So, unit representatives should plan on making presentations to these groups as their fall season develops.

### Grants
The rules for obtaining grants are relatively simple. Be very specific in your request. Keep it as narrow as possible so the proper focus remains in place. Follow all the grant guidelines to the letter. Be persistent but polite. Answer any follow-up questions promptly. At the conclusion, even if the grantor does not require a summary document, write a thank you letter. A community relations person, CAP public affairs officer, etc., should be included to coordinate grant applications.

It is recommended that anyone involved in unit funding refer to CAPR 173-4 Section C. This portion of the regulation very specifically lists what is not permitted for any funding effort. It also lists specifically allowed fund raising activities.

The Air Force Association provides funding to CAP to provide quarterly $250 grants to CAP units and CAP educator members to promote aerospace and STEM education.

## 8. AE POA
CAPP 50-1 8. The AE Plan of Action is the yearly plan for conducting aerospace education. Your AE Plan of Action is a map for future aerospace education activities the unit hopes to accomplish during the upcoming fiscal year. The plan should include the high-priority aspects of your aerospace education program that require your attention to accomplish.

At the squadron level, the AEPOA is developed by the AEO and signed by the unit commander. The AEOs should discuss the AEPOA with their commanders during the development phase to obtain opinions and garner their support.

The process starts with the squadron aerospace education officer completing AE Plan of Action. Squadron AEO’s may want to add action items that carry a positive impact on the squadrons AE program such as SpecialtyTrack growth, using AEX and STEM programs. The unit POA is provided to the Group (in Wings with Groups), and then to the wing or straight to the wing (in wings without groups).

CAPR 50-1 10. Each wing DAE will develop an AE Plan of Action (AE POA) in eServices each year. This plan represents the AE programs and activities the wing plans to conduct in the upcoming year. The plan will consist of four parts:
1. Focus the AEOs on accomplishing the squadron AE Achievement Award;
1. Remind the squadron AEOs to complete the AE Annual Activity Report at the end of each fiscal year;
1. Indicate on the AE POA in eServices, in the areas provided, the activities the wing plans to complete; and
1. Complete the Above and Beyond section of the plan for additional areas, events, or activities the wing conducted that are not covered in part three.

The Wing Commander will approve the AE POA in eServices. Regions will review the wing AE POA.

## 9. AE Activity Report
CAPP 50-1 9. CAPP 50-1August 2018599. AE Activity ReportEach squadron will complete the online AE Activity Report in eServices every year. This report records all of the unit’s accomplishments for the past fiscal year. Groups and wings will review the consolidated activity reports they receive from their squadrons, with the group and wing commanders approving their squadrons’ or groups’ reports. Regions will then review the wing submissions.

The AE Activity Report contains all the information pertaining to your unit’s aerospace education activities during the ending fiscal year. Providing information on the activities in the squadron, group, wing and region supports the CAP aerospace education mission, encourages additional funding from government and private sources, enhances the quality of our program locally and nationally while demonstrating the strength of each unit. It is your primary tool in tracking and reporting aerospace activities to your commander. It is also the only source of information used by Region Command when selecting the wing that earned the annual Aerospace Education Mission Award.

CAPR 50-1 11. Wing DAEs are responsible for monitoring and tracking the completion of squadron AE Annual Activity Report in eServices. The report records all the unit’s accomplishments for the past fiscal year. The wing DAE will consolidate the inputs to complete the wing AE Annual Activity Report. The Commander will approve the report. Regions will then review the wing submissions.

## 10. Inspection Preparation
CAPP 50-1 p. 66: The CAP Regulation governing the inspection process is explained in great detail in CAPR 20-3. Also see the Inspection Knowledge Base (IKB) in the Inspector General module.

The Subordinate Unit Inspection is a review of a unit’s program management, mission accomplishment and regulatory compliance. The AE program has been greatly simplified with the availability of electronic filing of the Plan of Action and Activity Report. SUIs are expected to occur every 24 months. Between SUIs, a unit conducts a self-inspection. The questions remain the same as for an SUI. If a unit exceeds 27 months between SUI visits, the Wing Commander must suspend the unit’s activities until the SUI can be completed. For the exact schedule of a unit SUI, there is an inspection program plan of action for all the subordinate units each year developed by the IG with the concurrence of the Wing Commander.

CAPR 50-1 12.2. Subordinate Unit Inspections – SUI. Squadrons and groups will be inspected regularly, applying the questions and procedures regarding the SUI found in Attachment 1 of this regulation.

CAPR 50-1 Attachment 1 - compliance elements:
- Did the AEO and the commander discuss future AE activities for the upcoming year? AEO will provide notes or audio/video record of discussion between AEO and CC about future AE activities.
- Does the unit have an Internal Aerospace Education program? Unit will submit examples of AE events (presentations, workshops, other activities, etc.) agendas which will include date and topic presented, activity done, etc.
- Does the unit have an external Aerospace Education program? Unit will submit examples of cooperative events with either schools or community organizations, such as civic or youth groups, which resulted in the promotion of Aerospace Education.
NOTE: If unable to provide documentation then provide documentation of attempts to establish cooperative events to promote Aerospace Education.
- Did the unit complete an AE Annual Activity Report IAW CAP regulations? Unit will submit a copy of the AE Annual Activity Report.
- Are AEOs assigned to the duty position enrolled in the AE specialty track, unless they have already achieved the AE master rating? Unit will provide a list of AEOs enrolled in the AE specialty track from Member Reports.
