# Glider Resources
![CAP Glider](https://bloximages.chicago2.vip.townnews.com/capemaycountyherald.com/content/tncms/assets/v3/editorial/6/fe/6fe3b9de-5d24-52c3-ac49-c1ba0cd58c8b/555be65c30038.image.jpg "A CAP Glider")

- [Best of Gliding (2015)](https://www.youtube.com/watch?v=bUWjvhmOrUg)
- [CAP Gliders](https://www.gocivilairpatrol.com/programs/emergency-services/aircraft-operations/aircraft/gliders)
- [FAA Glider Flying Handbook (links)](https://www.faa.gov/regulations_policies/handbooks_manuals/aircraft/glider_handbook/)
- [FAA Glider Flying Handbook (PDF)](https://www.faa.gov/regulations_policies/handbooks_manuals/aircraft/glider_handbook/media/faa-h-8083-13a.pdf)
- [How can gliders fly without propulsion](https://www.youtube.com/watch?v=b4YrpmhgNGs)
- [Quiz: Glider Flying Techniques](https://studentpilotnews.com/2019/02/04/quiz-glider-flying/)
- [Soaring Society of America](https://www.ssa.org/)
- [SSA Cadet Introductory Membership](https://cadet.ssa.org/)
- [SSA Junior Soaring](https://www.ssa.org/junior-soaring/)
