# Civil Air Patrol (CAP) Aerospace Resources

Nick Livingston's notes for aerospace education.

Created based on https://gitlab.com/pages/mkdocs/-/blob/master/.gitlab-ci.yml

The site is available at https://nblivingston.gitlab.io/aerospace .
